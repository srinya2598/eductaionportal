import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {UserClass} from '../user-class';
import {Router} from '@angular/router';
import {HttpService} from '../service/http.service';
import {map} from 'rxjs/operators';
import {QuestionClass} from '../question-class';

@Injectable({
  providedIn: 'root'
})
export class Middleware {
  private userSuject: BehaviorSubject<UserClass>;
  public user: Observable<UserClass>;

  constructor(private httpService: HttpService,
              private router: Router
  ) {
    // @ts-ignore
    this.userSuject = new BehaviorSubject<UserClass>(JSON.parse(localStorage.getItem('user')));
    this.user = this.userSuject.asObservable();
  }

  public get userValue(): UserClass {
    return this.userSuject.value;
  }

  register(userData: UserClass) {
    if (!userData) {


      return;
    }

    return this.httpService.post('/users/adduser', userData);

  }

  login(email: any, password: any) {

    return this.httpService.post('/users/auth', {email, password}).pipe(map(user => {
      localStorage.setItem('user', JSON.stringify(user));
      this.userSuject.next(user);
      return user;
    }));
  }

  logout() {
    // remove user from local storage and set current user to null
    localStorage.removeItem('user');
    // @ts-ignore
    this.userSuject.next(null);
    this.router.navigate(['']);
  }

  addQuestion(question: QuestionClass) {
    this.httpService.post('/ques/addQues', question).subscribe(res => {

    }, (error) => {

    });

  }

  questionsFetched() {
    console.log('questions are about to be fetched');
    return this.httpService.get('/ques');

  }
}
