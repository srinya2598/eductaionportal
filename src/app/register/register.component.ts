import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {Middleware} from '../middleware/middleware';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  form!: FormGroup ;
  loading = false;
  submitted = false;

  constructor(private router: Router,
              private formBuilder: FormBuilder, private middleware: Middleware) {

  }

  ngOnInit(): void {
    this.form = new FormGroup({
      fName: new FormControl(null, [Validators.required]),
      lName: new FormControl(null, [Validators.required]),
      email: new FormControl(null, [Validators.required, Validators.email]),
      password: new FormControl(null, [Validators.required])
    });
  }

  get f() {
    return this.form.controls;
  }

  onSubmit() {
    this.submitted = true;
    if (this.form.invalid) {
      return;
    }
    this.loading = true;

    // @ts-ignore
    this.middleware.register(this.form.value).subscribe(data => {

      this.router.navigate(['/home']);
    }, (error) => {

      this.loading = false;
    });

  }
  cancel(){
    this.router.navigate(['']);
  }
}
