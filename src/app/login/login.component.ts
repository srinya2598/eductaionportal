import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {Middleware} from '../middleware/middleware';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm!: FormGroup;
  loading = false;
  submitted = false;


  constructor(private router: Router, private middleware: Middleware, private  formBuilder: FormBuilder) {

  }

  ngOnInit(): void {
    this.loginForm = new FormGroup({
      email: new FormControl(null, [Validators.required, Validators.email]),
      password: new FormControl(null, [Validators.required]),
    });
  }

  get f() {
    return this.loginForm.controls;
  }

  onSubmit() {
    this.submitted = true;

    if (this.loginForm.invalid) {
      return;
    }
    this.loading = true;
    this.middleware.login(this.f.email.value, this.f.password.value).subscribe(data => {

      if (data.length) {
        this.router.navigate(['/home']);
      }

    }, (error) => {

      this.loading = false;
    });

  }

  register() {
    this.router.navigate(['/register']);

  }
}
