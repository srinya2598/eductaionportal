export enum Constants {
  USER_ID = 'user-id',
  AUTH_TOKEN = 'auth-token',
  VERIFIED = 'verified'
}
