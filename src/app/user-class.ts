export class UserClass {
  'id' ?: number;
  'fName': string;
  'lName': string;
  'email': string;
  'password': string;
}
