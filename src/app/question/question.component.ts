import {Component, OnInit, ViewChild} from '@angular/core';
import {Observable} from 'rxjs';


import {ToastrService} from 'ngx-toastr';
import {ModalDirective} from 'ngx-bootstrap/modal';
import {QuestionClass} from '../question-class';
import {Middleware} from '../middleware/middleware';


@Component({
  selector: 'app-question',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.css']
})
export class QuestionComponent implements OnInit {
  isQuestionCardShow = false;
  totalAnswered = 0;
  rightAnswer!: number;
  marks = 0;
  d!: number;
  reattemps = 0;
  questionObj = new QuestionClass();
  @ViewChild('submitModal') submitModal!: ModalDirective;
  @ViewChild('addQuestionModal') addQuestionModal!: ModalDirective;
  @ViewChild('answerModal') answerModal!: ModalDirective;
  @ViewChild('questionForm') questionForm: any;
  @ViewChild('questionTest') questionTest: any;
  @ViewChild('score') score!: ModalDirective;

  constructor(private toastr: ToastrService, private middleware: Middleware) {

    this.middleware.questionsFetched().subscribe((res: QuestionClass[]) => {

      this.allQuestions = res;
    });

  }

  answerArray = [];

  allQuestions: any = [];


  reviewTest(): void {
    this.rightAnswer = 0;
    this.totalAnswered = 0;
    for (let i = 0; i < this.allQuestions.length; i++) {
      if ('selected' in this.allQuestions[i] && (this.allQuestions[i].selected != null)) {
        this.totalAnswered++;
        if (this.allQuestions[i].selected === this.allQuestions[i].answer) {
          this.rightAnswer++;
        }
      }

    }
    this.submitModal.show();

  }

  submitTes(): void {
    if (this.reattemps <= 3) {
      this.d = this.reattemps + 1;
      this.marks = (this.rightAnswer * 4) / this.d;
    } else {
      this.marks = 0;
    }
    this.score.show();
  }

  startQuiz(): void {

    this.questionTest.reset();
    this.isQuestionCardShow = true;

  }

  HomePage(): void {
    this.isQuestionCardShow = false;
  }

  addQuestion(): void {
    this.addQuestionModal.show();
  }

  submitAddQuestion(): void {
    const quesTemp = JSON.parse(JSON.stringify(this.questionObj));

    this.questionForm.reset();

    this.addQuestionModal.hide();
    this.middleware.addQuestion(quesTemp);

    this.middleware.questionsFetched().subscribe((res: QuestionClass[]) => {

      this.allQuestions = res;
    });


  }

  checkAnswers(): void {
    this.submitModal.hide();
    this.answerModal.show();
  }

  reattempt(): void {

    this.reattemps = this.reattemps + 1;

    this.questionTest.reset();


  }

  ngOnInit() {


  }

  logout() {
    this.middleware.logout();

  }
}
